<?php

namespace CodingPaws\SafeKeyGenerate;

use CodingPaws\SafeKeyGenerate\Console\KeyGenerateCommand;
use Illuminate\Support\ServiceProvider;

class SafeKeyServiceProvider extends ServiceProvider
{
  public function boot()
  {
    if ($this->app->runningInConsole()) {
      $this->app->extend('command.key.generate', function () {
        return new KeyGenerateCommand();
      });
    }
  }
}
