# Laravel Safe `key:generate`

The `php artisan key:generate` command in Laravel overwrites the `APP_KEY` by default on every execution, except in production. As a developer, you may not want to accidentally overwrite your application key though, leading to invalid cookies and sessions. This package secures `key:generate` by preventing accidental key overwrites when the `APP_KEY` is already set in the `.env` file.
